<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'manage'], function () {
    Route::group(['middleware' => 'access'],function(){
        Route::get('shops','Api\ShopController@index');
        Route::get('shops/{id}','Api\ShopController@show');
        Route::post('shops','Api\ShopController@store');
        Route::put('shops/{id}','Api\ShopController@update');
        Route::delete('shops/{id}','Api\ShopController@destroy');

        Route::get('products','Api\ProductController@index');
        Route::get('shops/{sid}/products','Api\ProductController@index_by_shop');
        Route::get('products/{id}','Api\ProductController@show');
        Route::post('products','Api\ProductController@store');
        Route::put('products/{id}','Api\ProductController@update');
        Route::delete('products/{id}','Api\ProductController@destroy');

        Route::get('purchases','Api\PurchaseController@index');
        Route::post('purchases','Api\PurchaseController@store');
        Route::put('purchases/{id}','Api\PurchaseController@update');

        Route::get('sales','Api\SaleController@index');

        Route::get('employees','Api\EmployeeController@index');

        Route::get('providers','Api\ProviderController@index');

        Route::get('customers','Api\CustomerController@index');
    });
    Route::get('categories','Api\CategoryController@index');
});

Route::get('checkaccess', 'Api\SecurityController@checkaccess')->name('apiaccess')->middleware('access');

Route::group(['prefix' => 'secure'], function () {
    Route::post('login', 'Api\SecurityController@login')->name('apilogin');
    Route::post('register', 'Api\SecurityController@register')->name('apiregister');
    Route::get('key', 'Api\SecurityController@newClientKey')->name('apikey');
});
