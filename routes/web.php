<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Resources\CountryResource;
use App\Model\Country;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

Route::get('/', function () {
    return view('welcome');
});

Route::get('manage', function () {
    return view('shops.home');
});

Route::group(['prefix'=>'shop'],function(){
    Route::get('login', function (){
        return view('shops.auth.login');
    })->name('shoplogin');

    Route::get('register', function (){
        $countries = CountryResource::collection(Country::all());
        return view('shops.auth.register')->with('countries',$countries);
    })->name('shopregister');

    Route::get('app', function (){
        return view('shops.welcome');
    })->name('shopwelcome');
});


Route::get('login', function (){
    return 'comming soon';
})->name('login');

Route::get('register', function (){
    $countries = CountryResource::collection(Country::all());
    return 'comming soon';
})->name('register');

Route::get('request',function(Request $request){
    return Response::create($request,200);
});
