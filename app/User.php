<?php

namespace App;

use App\Model\Sale;
use App\Model\Shop;
use App\Model\Subscription;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','country_id','api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function shops()
    {
        return $this->belongsToMany(Shop::class,'roles', 'user_id', 'shop_id')->withPivot('can_read_shops','can_edit_shops','can_delete_shops','can_create_products','can_read_products','can_edit_products','can_delete_products','can_create_sales','can_read_sales','can_edit_sales','can_delete_sales','can_create_purchases','can_read_purchases','can_edit_purchases','can_delete_purchases','can_create_users','can_read_users','can_edit_users','can_delete_users')->withTimestamps();
    }
}
