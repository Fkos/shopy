<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('can_edit_shop','App\Policies\PermissionPolicy@edit_shop');
        Gate::define('can_read_shop','App\Policies\PermissionPolicy@read_shop');
        Gate::define('can_delete_shop','App\Policies\PermissionPolicy@delete_shop');

        Gate::define('can_edit_sale','App\Policies\PermissionPolicy@edit_sales');
        Gate::define('can_read_sale','App\Policies\PermissionPolicy@read_sales');
        Gate::define('can_create_sale','App\Policies\PermissionPolicy@create_sales');
        Gate::define('can_delete_sale','App\Policies\PermissionPolicy@delete_sales');

        Gate::define('can_edit_purchase','App\Policies\PermissionPolicy@edit_purchases');
        Gate::define('can_read_purchase','App\Policies\PermissionPolicy@read_purchases');
        Gate::define('can_create_purchase','App\Policies\PermissionPolicy@create_purchases');
        Gate::define('can_delete_purchase','App\Policies\PermissionPolicy@delete_purchases');

        Gate::define('can_edit_product','App\Policies\PermissionPolicy@edit_products');
        Gate::define('can_read_product','App\Policies\PermissionPolicy@read_products');
        Gate::define('can_create_product','App\Policies\PermissionPolicy@create_products');
        Gate::define('can_delete_product','App\Policies\PermissionPolicy@delete_products');

        Gate::define('can_edit_user','App\Policies\PermissionPolicy@edit_users');
        Gate::define('can_read_user','App\Policies\PermissionPolicy@read_users');
        Gate::define('can_create_user','App\Policies\PermissionPolicy@create_users');
        Gate::define('can_delete_user','App\Policies\PermissionPolicy@delete_users');
    }
}
