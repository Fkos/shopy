<?php

namespace App\Policies;

use App\Model\Shop;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function edit_shop($user, $shop){
        $result = DB::select('select can_edit_shops from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_edit_shops){
            return true;
        }
        return false;
    }

    public function read_shop($user, $shop){
        $result = DB::select('select can_read_shops from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_read_shops == 1){
            return true;
        }
        return false;
    }

    public function delete_shop($user, $shop){
        $result = DB::select('select can_delete_shops from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_delete_shops){
            return true;
        }
        return false;
    }

    public function read_product($user, $shop){
        $result = DB::select('select can_read_products from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_read_products){
            return true;
        }
        return false;
    }

    public function edit_product($user, $shop){
        $result = DB::select('select can_edit_products from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_edit_products){
            return true;
        }
        return false;
    }

    public function create_product($user, $shop){
        $result = DB::select('select can_create_products from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_create_products){
            return true;
        }
        return false;
    }

    public function delete_product($user, $shop){
        $result = DB::select('select can_delete_products from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_delete_products){
            return true;
        }
        return false;
    }

    public function read_sale($user, $shop){
        $result = DB::select('select can_read_sales from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_read_sales){
            return true;
        }
        return false;
    }

    public function edit_sale($user, $shop){
        $result = DB::select('select can_edit_sales from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_edit_sales){
            return true;
        }
        return false;
    }

    public function create_sale($user, $shop){
        $result = DB::select('select can_create_sales from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_create_sales){
            return true;
        }
        return false;
    }

    public function delete_sale($user, $shop){
        $result = DB::select('select can_delete_sales from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_delete_sales){
            return true;
        }
        return false;
    }

    public function read_purchase($user, $shop){
        $result = DB::select('select can_read_purchases from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_read_purchases){
            return true;
        }
        return false;
    }

    public function edit_purchase($user, $shop){
        $result = DB::select('select can_edit_purchases from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_edit_purchases){
            return true;
        }
        return false;
    }

    public function create_purchase($user, $shop){
        $result = DB::select('select can_create_purchases from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_create_purchases){
            return true;
        }
        return false;
    }

    public function delete_purchase($user, $shop){
        $result = DB::select('select can_delete_purchases from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_delete_purchases){
            return true;
        }
        return false;
    }

    public function read_user($user, $shop){
        $result = DB::select('select can_read_users from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_read_users){
            return true;
        }
        return false;
    }

    public function edit_user($user, $shop){
        $result = DB::select('select can_edit_users from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_edit_users){
            return true;
        }
        return false;
    }

    public function create_user($user, $shop){
        $result = DB::select('select can_create_users from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_create_users){
            return true;
        }
        return false;
    }

    public function delete_user($user, $shop){
        $result = DB::select('select can_delete_users from roles where shop_id = :shop and user_id = :user limit 1',['shop'=>$shop,'user'=>$user]);
        if ($result != null && $result[0]->can_delete_users){
            return true;
        }
        return false;
    }
}
