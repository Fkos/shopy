<?php

namespace App\Model;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function shops(){
        return $this->hasMany(Shop::class);
    }

    public function customers(){
        return $this->hasMany(Customer::class);
    }

    public function users(){
        return $this->hasMany(User::class);
    }

    public function administrator(){
        return $this->hasMany(Administrator::class);
    }
}
