<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function access()
    {
        return $this->hasMany(Access::class);
    }
}
