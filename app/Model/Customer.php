<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User;
use Illuminate\Notifications\Notifiable;

class Customer extends User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function orders(){
        return $this-> hasMany(Order::class);
    }

    public function sales(){
        return $this-> hasMany(Sale::class);
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }
}
