<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function purchases(){
        return $this->hasMany(Purchase::class);
    }

    public function sales(){
        return $this->hasMany(Sale::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}
