<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Access extends Model
{
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
