<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public function provider(){
        return $this->belongsTo(Provider::class);
    }

    public function Product()
    {
        return $this->belongsTo(Product::class);
    }
}
