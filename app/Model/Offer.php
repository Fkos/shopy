<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    public function subscriptions(){
        return $this->hasMany(Subscription::class);
    }

    public function administrator(){
        return $this->belongsTo(Administrator::class);
    }
}
