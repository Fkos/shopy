<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Shop extends Model
{
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'roles', 'shop_id', 'user_id')->withPivot('can_read_shops','can_edit_shops','can_delete_shops','can_create_products','can_read_products','can_edit_products','can_delete_products','can_create_sales','can_read_sales','can_edit_sales','can_delete_sales','can_create_purchases','can_read_purchases','can_edit_purchases','can_delete_purchases','can_create_users','can_read_users','can_edit_users','can_delete_users')->withTimestamps();
    }

    public function purchases(){
        return $this->hasManyThrough(Purchase::class,'App\Model\Product');
    }

    public function sales(){
        return $this->hasManyThrough(Sale::class,'App\Model\Product');
    }

    public function customers(){
        return $this->hasManyThrough(Customer::class,'App\Model\Sales');
    }
}
