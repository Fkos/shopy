<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    public function purchases (){
        return $this->hasMany(Purchase::class);
    }
}
