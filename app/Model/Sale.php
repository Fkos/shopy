<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Sale extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
