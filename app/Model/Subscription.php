<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Subscription extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

}
