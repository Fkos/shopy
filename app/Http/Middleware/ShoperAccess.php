<?php

namespace App\Http\Middleware;

use App\Model\Access;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Response;

class ShoperAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->cookie('access_token') == null){
            return Response::create('Bad access',401);
        }
        if($request->client_key == null){
            return Response::create(['message'=>'Missing client key'],422);
        }
        $access = Access::all()->where("token", $request->cookie('access_token'))->first();
        if($access == null){
            return Response::create('Bad access',401);
        }
        if($access->client->key != $request->client_key){
            $access->consecutive_bad_attempts ++;
            if($access->consecutive_bad_attempts >= 5){
                $access->delete();
            }else{
                $access->save();
            }
            return Response::create(['message'=>'Invalid Access'],401);
        }
        $access->consecutive_bad_attempts = 0;
        $access->last_call_at = Carbon::now();
        $access->save();
        return $next($request);
    }
}