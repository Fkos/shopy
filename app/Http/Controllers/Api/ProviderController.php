<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProviderResource;
use App\Model\Access;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProviderController extends Controller
{
    public function index(Request $request){
        $access = Access::where('token',$request->cookie('access_token'))->first();
        $providers = [];
        if ($access!=null) {
            foreach ($access->user->shops as $shop) {
                if ($shop->pivot->can_read_purchases) {
                    foreach($shop->purchases as $purchase)
                        array_push($providers,$purchase->provider);
                }
            }
            return Response::create(ProviderResource::collection($providers)->unique(),200);
        }
        return Response::create('Forbidden', 403);
    }
}
