<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PurchaseResource;
use App\Model\Access;
use App\Model\Product;
use App\Model\Provider;
use App\Model\Purchase;
use App\Model\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $access = Access::where('token',$request->cookie('access_token'))->first();
        $purchases = [];
        if ($access!=null) {
            foreach ($access->user->shops as $shop) {
                if ($shop->pivot->can_read_purchases) {
                    foreach($shop->purchases as $purchase)
                        array_push($purchases,$purchase);
                }
            }
            return Response::create(PurchaseResource::collection($purchases),200);
        }
        return Response::create('Forbidden', 403);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_by_shop($id)
    {
        $purchase = Purchase::all()->where('shop_id',$id);
        //Check if user can read purchases
        return Response::create($purchase,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Check if authenticated user can create purchase entry
        $rules = [
            'product_id' => 'required|numeric|exists:products,id',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
        ];
        if (isset($request->new_provider)){
            $rules['provider_name'] = 'required|string';
            $rules['provider_email'] = 'required|email';
            $rules['provider_phone'] = 'required|numeric';
        }else{
            $rules['provider_id'] = 'required|numeric|exists:providers,id';
        }

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()){
            return Response::create($validator->errors(),422);
        }
        $product = Product::find($request->product_id);
        $access = Access::all()->where("token", $request->cookie('access_token'))->first();
        $role = Role::all()->where('user_id',$access->user->id)->where('shop_id',$product->shop->id)->first();
        if ($role->can_create_purchases){
            $purchase = new Purchase();
            $purchase->product()->associate($product);
            $purchase->price = $request->price;
            $purchase->quantity = $request->quantity;
            if (isset($request->new_provider)){
                $provider = new Provider();
                $provider->name = $request->provider_name;
                $provider->email = $request->provider_email;
                $provider->phone = $request->provider_phone;
                $provider->save();
                $purchase->provider()->associate($provider);
            }else{
                $purchase->provider()->associate(Provider::find($request->provider_id));
            }
            $purchase->save();
            $product->number += $purchase->quantity;
            $product->save();
            return Response::create($purchase,201);
        }
        return Response::create('Forbidden',403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Check if authenticated user can read products in this shop
        $purchase = Purchase::find($id);
        return Response::create($purchase,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Check if authenticated user can update purchase entry
        $rules = [

        ];

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()){
            return Response::create($validator->errors(),422);
        }
        $purchase = Purchase::find($id);
        return Response::create($purchase,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Check if authenticated user can create purchase entry
        $purchase = Purchase::find($id);
        $purchase->delete();
        return Response::create('Deleted',200);
    }
}
