<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;
use App\Model\Access;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CustomerController extends Controller
{
    public function index(Request $request){
        $access = Access::where('token',$request->cookie('access_token'))->first();
        if ($access!=null) {
            $customers = [];
            foreach ($access->user->shops as $shop) {
                if ($shop->pivot->can_read_sales) {
                    foreach($shop->sales as $sale)
                        array_push($customers,$sale->customer);
                }
            }
            return Response::create(CustomerResource::collection($customers)->unique(),200);
        }
        return Response::create('Forbidden', 403);
    }
}
