<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Model\Access;
use App\Model\Product;
use App\Model\Role;
use App\Model\Shop;
use App\Model\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index(Request $request){
        $access = Access::where('token',$request->cookie('access_token'))->first();
        if ($access!=null) {
            $products = [];
            foreach ($access->user->shops as $shop) {
                if ($shop->pivot->can_read_products) {
                    foreach($shop->products as $product)
                    array_push($products,$product);
                }
            }
            return Response::create(ProductResource::collection($products),200);
        }
        return Response::create('Forbidden', 403);
    }

    public function index_by_shop($sid){
        //check if user is authorized to read product in the shop
        $products = Product::where('shop_id',$sid)->get();
        return Response::create(ProductResource::collection($products),200);
    }

    public function store (Request $request){
        //check if user is authorized to create product in the shop
        $rules = [
            'name'=>'required|string',
            'description'=>'required|string',
            'price'=>'required|numeric',
            'shop_id'=>'required|numeric|exists:shops,id',
            'category_id'=>'required|numeric|exists:categories,id',
        ];

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return Response::create($validator->errors(),422);
        }
        $access = Access::all()->where("token", $request->cookie('access_token'))->first();
        $role = Role::all()->where('user_id',$access->user->id)->where('shop_id',$request->shop_id)->first();
        if ($role->can_create_products){
            $product = new Product();
            $product->name = $request->name;
            $product->description = $request->description;
            $product->price = $request->price;
            $product->number = 0;
            $product->shop()->associate(Shop::find($request->shop_id));
            $product->category()->associate(Category::find($request->category_id));
            $product->save();
            return Response::create(new ProductResource($product),201);
        }
        return Response::create('Forbidden',403);
    }

    public function update(Request $request, $id){
        //check if user is authorized to edit product in the shop
        $rules = [
            'name'=>'string',
            'description'=>'string',
            'price'=>'numeric',
            'shop_id'=>'exists:shops,id',
            'category_id'=>'exists:categories,id',
        ];

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return Response::create($validator->errors(),422);
        }
        $access = Access::all()->where("token", $request->cookie('access_token'))->first();
        $product = Product::find($id);
        $role = Role::all()->where('user_id',$access->user->id)->where('shop_id',$request->shop_id)->first();
        if ($role != null && $role->can_edit_products) {
            if (isset($request->name) && $product->name != $request->name){
                $product->name = $request->name;
            }
            if(isset($request->description) && $product->description != $request->description) {
                $product->description = $request->description;
            }
            if (isset($request->price) && $product->price != $request->price) {
                $product->price = $request->price;
            }
            if (isset($request->shop_id) && $request->shop_id != null && $product->shop->id != $request->shop_id) {
                if ($role->can_delete_products){
                    $role2 = Role::all()->where('user_id',$access->user->id)->where('shop_id',$request->shop_id)->first();
                    if ($role2 != null && $role2->can_create_products){
                        $product->shop()->associate(Shop::find($request->shop_id));
                    }
                }
            }
            if (isset($request->category_id) && $request->category_id != null  && ($product->category->id != $request->category_id)){
                $product->category()->associate(Category::find($request->category_id));
            }
            $product->save();
            return Response::create(new ProductResource($product),200);
        }
        return Response::create('Forbidden',403);
    }

    public function show($id,$token){
        $product = Product::find($id);
        //check if user is authorized to read product in the shop
        return Response::create($product,200);
    }

    public function destroy(Request $request,$id){
        $product = Product :: find($id);
        if($product!= null){
            $access = Access::where('token',$request->cookie('access_token'))->first();
            if($access != null){
                $role = Role::where('user_id',$access->user->id)->where('shop_id',$product->shop_id)->first();
                if($role != null && $role->can_delete_products){
                    $product->delete();
                    return Response::create('',200);
                }
            }
            return Response::create('Forbidden',403);
        }
        return Response::create('Not Found',404);
    }
}
