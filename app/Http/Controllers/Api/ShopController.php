<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ShopResource;
use App\Model\Access;
use App\Model\Country;
use App\Model\Shop;
use App\Model\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $access = Access::all()->where('token',$request->cookie('access_token'))->first();
        return Response::create(ShopResource::collection($access->user->shops),200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'country_id' => 'required|numeric',
            'name' => 'required|string',
            'description' => 'required|string',
            'address' => 'required|string',
            'email' => 'required|email|unique:shops,email',
            'phone' => 'required|numeric|unique:shops,phone',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return Response::create($validator->errors(), 422 );
        }
        $access = Access::all()->where("token", $request->cookie('access_token'))->first();
        $shop = new Shop();
        $shop->country()->associate(Country::find($request->country_id));
        $shop->name = $request->name;
        $shop->description = $request->description;
        $shop->address = $request->address;
        $shop->email = $request->email;
        $shop->phone = $request->phone;
        $shop->latitude = $request->latitude;
        $shop->longitude = $request->longitude;
        $shop->save();
        $shop->users()->attach($access->user->id);

        return Response::create(new ShopResource($shop), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // if(Gate::allows('can_read_shop',Auth::id(),$id)){
            $s = Shop::find($id);
            if($s != null){
            $shop = new ShopResource($s);
            return Response::create($shop, 200);
            }
            return Response::create('', 404);
        // }

        // return (Shop::find($id)!= null)?Response::create($id.' '.Auth::id(),403):Response::create('',404);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // if(Gate::allows('can_edit_shop',[Auth::id(),$id])){
            $rules = [
                'id' => 'nullable|numeric|exists:shops,id',
                'country_id' => 'nullable|numeric',
                'name' => 'nullable|string',
                'description' => 'nullable|string',
                'address' => 'nullable|string',
                'email' => 'nullable|email',
                'phone' => 'nullable|numeric',
                'latitude' => 'nullable|double',
                'longitude' => 'nullable|double',
            ];

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()){
                return Response::create($validator->errors(), 422 );
            }

            $shop = Shop::find($id);
            if($shop != null){
            if(isset($request->country_id))
            $shop->country()->associate(Country::find($request->country_id));
            if(isset($request->name))
            $shop->name = $request->name;
            if(isset($request->description))
            $shop->description = $request->description;
            if(isset($request->address))
            $shop->address = $request->address;
            if(isset($request->email))
            $shop->email = $request->email;
            if(isset($request->phone))
            $shop->phone = $request->phone;
            if(isset($request->latitude))
            $shop->latitude = $request->latitude;
            if(isset($request->longitude))
            $shop->longitude = $request->longitude;
            $shop->save();
            return Response::create($shop, 200);
            }
            return Response::create('', 404);
        // }
        // return (Shop::find($id)!= null)?Response::create('',403):Response::create('',404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $access = Access::where('token',$request->cookie('access_token'))->first();
        if($access != null){
            $role = Role::where('user_id',$access->user->id)->where('shop_id',$id)->first();
            if($role != null && $role->can_delete_shops){
                $shop = Shop::find($id);
                if($shop != null){
                    $shop->users()->detach();
                    $shop->delete();
                    return Response::create('', 200);
                }
                return Response::create('Not Found', 404);
            }
        }
        return Response::create('Forbidden', 403);
    }
}
