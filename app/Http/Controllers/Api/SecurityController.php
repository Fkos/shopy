<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AccessResource;
use App\Http\Resources\ClientResource;
use App\Model\Client;
use App\User;
use App\Model\Access;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SecurityController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'numeric', 'unique:users'],
            'country' => ['required', 'numeric','exists:countries,id'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if($validator->fails()){
            return Response::create($validator->errors(), 422);
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->country_id = $request->country;
        $user->password = Hash::make($request->password);
        $user->save();
        return Response::create($user, 201);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email','exists:users'],
            'password' => ['required', 'string'],
            'remember' => ['nullable'],
            'client_key' => ['required','string'],
        ]);

        if($validator->fails()){
            return Response::create($validator->errors(), 422);
        }
        $client = Client::all()->where('key',$request->client_key)->first();
        if($client!=null){
            $user = User::all()->where('email',$request->email)->first();
            if ($user != null) {
                if (Hash::check($request->password, $user->password)) {
                    $access = new Access();
                    $access->token = Str::random(80);
                    if(isset($request->remember)){
                        $access->expire_at = Carbon::now()->add('day',365);
                    }else{
                        $access->expire_at = Carbon::now()->add('hour',2);
                    }
                    $access->last_call_at = Carbon::now();
                    $access->user()->associate($user);
                    $access->client()->associate($client);
                    $access->save();
                    return Response::create(new AccessResource($access), 200);
                }
            }
            return Response::create(['message'=>'Invalid credentials'], 401);
        }

        return Response::create(['message'=>'Invalid client'],401);
    }

    public function newClientKey(){
        $client = new Client();
        $client->key = Str::random(50);
        $client->last_call_at = Carbon::now();
        $client->save();
        return Response::create(new ClientResource($client),201);
    }

    public function checkAccess(Request $request){
        $access = Access::where('token',$request->cookie('access_token'))->first();
        $user = $access->user;
        return Response::Create($user,200);
    }
}
