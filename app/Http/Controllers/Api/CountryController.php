<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CountryResource;
use App\Model\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index () {
        $countries = Country::all();
        return CountryResource::collection($countries);
    }
}
