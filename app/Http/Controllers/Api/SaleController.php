<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SaleResource;
use App\Model\Access;
use App\Model\Sale;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class SaleController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $access = Access::where('token',$request->cookie('access_token'))->first();
        if ($access!=null) {
            $sales = [];
            foreach ($access->user->shops as $shop) {
                if ($shop->pivot->can_read_sales) {
                    foreach($shop->sales as $sale)
                        array_push($sales,$sale);
                }
            }
            return Response::create(SaleResource::collection($sales),200);
        }
        return Response::create('Forbidden', 403);
    }

    public function index_by_shop($id)
    {
        $sale = Sale::all()->where('shop_id',$id);
        //Check if user can read Sales
        return Response::create($sale,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Check if authenticated user can create Sale entry
        $rules = [

        ];

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()){
            return Response::create($validator->errors(),422);
        }
        $sale = new Sale();
        return Response::create($sale,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sid,$id)
    {
        //Check if authenticated user can read products in this shop
        $sale = Sale::find($id);
        return Response::create($sale,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Check if authenticated user can update Sale entry
        $rules = [

        ];

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()){
            return Response::create($validator->errors(),422);
        }
        $sale = Sale::find($id);
        return Response::create($sale,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Check if authenticated user can create Sale entry
        $sale = Sale::find($id);
        $sale->delete();
        return Response::create('Deleted',200);
    }
}
