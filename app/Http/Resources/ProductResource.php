<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'number' => $this->number,
            'shop' => $this->shop->name,
            'sales' => $this->sales->sum('quantity'),
            'purchases' => $this->purchases->sum('quantity'),
            'description' => $this->description,
            'category_id' => $this->category->id,
            'shop_id' => $this->shop->id,
        ];
        return $result;
    }
}
