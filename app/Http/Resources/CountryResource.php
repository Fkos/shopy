<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryResource extends JsonResource
{
    /**
    * Transform the resource into an array.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return array
    */
    
    public function toArray($request)
    {
        $result = [
            $this->phone_code,
            $this->name,
            $this->iso,
            $this->id,
        ];
        return $result;
    }
}
