@extends('layouts.admin')

@section('content')
<div class="w3-container w3-padding-32">
    <h1 class="w3-xxxlarge w3-text-blue"><b>{{ __('Login') }}</b></h1>
    <ul class="w3-alert w3-margin-bottom-16 w3-red" id="alerts"></ul>
    <form method="POST" id="login">
        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

        <div>
            <input id="email" type="email" class="w3-input w3-margin-bottom @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group row">
            <label for="password">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="w3-input w3-margin-bottom @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <button id="loginbtn" type="submit" class="w3-button w3-blue">
            {{ __('Login') }}
        </button>
    </form>
</div>
<script>
    document.getElementById("login").addEventListener("submit",function (e) {
        e.preventDefault();
        var email = document.getElementById("email");
        var password = document.getElementById("password");
        var loginbtn = document.getElementById("loginbtn");
        var alerts = document.getElementById("alerts");
        var remember = document.getElementById("remember");
        if(email.checkValidity() && password.checkValidity()){
            let xhr = new XMLHttpRequest();
            xhr.open("post","{{ route('apilogin') }}",true);
            xhr.onreadystatechange = function (){
                if(this.readyState===1){
                    loginbtn.setAttribute("disabled",true);
                }
                if(this.readyState===4){
                    let response = JSON.parse(this.responseText);
                    if(this.status===200){
                        console.log(response);
                        // localStorage.setItem('token',response.)
{{--                        window.location.href="{{env("APP_URL")}}/manage";--}}
                    }else if(this.status >= 400){
                        if(response.email !== undefined){
                            for(i=0;i<response.email.length;i++){
                                alerts.innerHTML= "";
                                var alertel = document.createElement("li");
                                alertel.innerHTML = response.email[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        console.log(response);
                    }
                }
                loginbtn.removeAttribute("disabled");
            }
            xhr.setRequestHeader("content-type","application/x-www-form-urlencoded");
            if(remember.checked === true){
                xhr.send("email="+email.value+"&password="+password.value);
            }else{
                xhr.send("email="+email.value+"&password="+password.value
                    +"&remember="+remember.value);
            }
        }else{
            console.log("invalid");
        }
    });
</script>
@endsection
