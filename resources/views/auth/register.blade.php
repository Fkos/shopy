@extends('layouts.app')

@section('content')
<div class="w3-padding-32 w3-container">
    
        
    <h1 class="w3-xxxlarge w3-text-blue"><b>{{ __('Register') }}</b></h1>
    <form method="POST" action="{{ route('apiregister') }}">
        @csrf

            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

            <div>
                <input id="name" type="text" class="w3-input w3-margin-bottom @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                @error('name')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                @enderror
            </div>

            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div>
                <input id="email" type="email" class="w3-input w3-margin-bottom @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                @enderror
            </div>
            <label for="country" class="col-md-4 col-form-label text-md-right">{{__('Country')}}</label>
            <div>
                <select class="w3-input w3-margin-bottom @error('phone') is-invalid @enderror" name="country" id="country" required autocomplete="country">
                    <option selected disabled>{{__('Choose a Country')}}</option>
                    @foreach($countries as $country)
                    <option value="{{$country->id}}" @if(old('country')==$country->id)selected @endif>{{__($country->name)}}</option>
                    @endforeach
                </select>
            </div>
            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

            <div>
                <input id="phone" type="phone" class="w3-input w3-margin-bottom @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">

                @error('phone')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                @enderror
            </div>
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div>
                <input id="password" type="password" class="w3-input w3-margin-bottom @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                @enderror
            </div>
            <label for="password-confirm">{{ __('Confirm Password') }}</label>

            <div>
                <input id="password-confirm" type="password" class="w3-input w3-margin-bottom" name="password_confirmation" required autocomplete="new-password">
            </div>
            <div>
                <button type="submit" class="w3-button w3-blue">
                    {{ __('Register') }}
                </button>
            </div>
    </form> 
</div>
@endsection
