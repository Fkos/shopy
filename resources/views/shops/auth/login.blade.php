@extends('layouts.admin')

@section('content')
<div class="w3-container w3-padding-32">
    <h1 class="w3-xxxlarge w3-text-blue"><b>{{ __('Login') }}</b></h1>
    <ul class="w3-alert w3-margin-bottom-16 w3-red" id="alerts"  style="max-width: 600px"></ul>
    <form method="POST" id="login">
        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

        <div>
            <input id="email" type="email" class="w3-input w3-margin-bottom" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
        </div>

        <label for="password">{{ __('Password') }}</label>

        <div>
            <input id="password" type="password" class="w3-input w3-margin-bottom" name="password" required autocomplete="current-password">
        </div>
        <div>
            <label for="remember">{{__('Remember me?')}}</label> <input id="remember" name="remember" class="w3-check w3-margin-bottom " type="checkbox" checked>
        </div>
        <button id="loginbtn" type="submit" class="w3-button w3-blue">
            {{ __('Login') }}
        </button>
    </form>
</div>
<script>
    document.getElementById("login").addEventListener("submit",function (e) {
        e.preventDefault();
        var email = document.getElementById("email");
        var password = document.getElementById("password");
        var loginbtn = document.getElementById("loginbtn");
        var alerts = document.getElementById("alerts");
        var remember = document.getElementById("remember");
        if(email.checkValidity() && password.checkValidity()){
            let xhr = new XMLHttpRequest();
            xhr.open("post","{{ route('apilogin') }}",true);
            xhr.onreadystatechange = function (){
                if(this.readyState===1){
                    loginbtn.setAttribute("disabled",true);
                }
                if(this.readyState===4){
                    console.log(this.response);
                    if(this.status===200){
                        setCookie("access_token",JSON.parse(this.response).token);
                        window.location.href="{{env("APP_URL")}}/shop/app";
                    }else if(this.status >= 400){
                        let response = JSON.parse(this.response);
                        alerts.innerHTML= "";

                        if(response.email !== undefined){
                            for(i=0;i<response.email.length;i++){
                                var alertel = document.createElement("li");
                                alertel.innerHTML = response.email[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (response.password !== undefined) {
                            for (i = 0; i < response.password.length; i++) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = response.password[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (response.message !== undefined) {
                            var alertel = document.createElement("li");
                            alertel.innerHTML = response.message;
                            alerts.appendChild(alertel);
                            if(response.message === "Invalid client"){
                                window.localStorage.removeItem("client_key");
                                document.location.reload();
                            }
                        }
                    }
                    loginbtn.removeAttribute("disabled");
                }
            }

            //send the login request
            xhr.setRequestHeader("content-type","application/x-www-form-urlencoded");
            var reqdata = "email=" + email.value + "&password=" + password.value;
            if(remember !== null && remember.checked === true){
                reqdata = reqdata + "&remember=" + remember.value;
            }
            reqdata = reqdata + "&client_key=" + window.localStorage.getItem("client_key");
            xhr.send(reqdata);
        }else{
            console.log("invalid");
        }
        console.log("trigger");
    });
</script>
@endsection
