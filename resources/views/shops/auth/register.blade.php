@extends('layouts.admin')

@section('content')
<div class="w3-padding-32 w3-container">


    <h1 class="w3-xxxlarge w3-text-blue"><b>{{ __('Register') }}</b></h1>
    <ul class="w3-alert w3-margin-bottom-16 w3-red" id="alerts"  style="max-width: 600px"></ul>
    <form id="register" method="POST" action="#">
        @csrf
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

            <div>
                <input id="name" type="text" class="w3-input w3-margin-bottom @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                @error('name')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                @enderror
            </div>

            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div>
                <input id="email" type="email" class="w3-input w3-margin-bottom @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                @enderror
            </div>
            <label for="country" class="col-md-4 col-form-label text-md-right">{{__('Country')}}</label>
            <div>
                <select class="w3-input w3-margin-bottom @error('phone') is-invalid @enderror" name="country" id="country" required autocomplete="country">
                    <option selected disabled>{{__('Choose a Country')}}</option>
                    @foreach($countries as $country)
                    <option value="{{$country->id}}" @if(old('country')==$country->id)selected @endif>{{__($country->name)}}</option>
                    @endforeach
                </select>
            </div>
            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

            <div>
                <input id="phone" type="phone" class="w3-input w3-margin-bottom @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">

                @error('phone')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                @enderror
            </div>
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div>
                <input id="password" type="password" class="w3-input w3-margin-bottom @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                @enderror
            </div>
            <label for="password-confirm">{{ __('Confirm Password') }}</label>

            <div>
                <input id="password-confirm" type="password" class="w3-input w3-margin-bottom" name="password_confirmation" required autocomplete="new-password">
            </div>
            <div>
                <button id="regbtn" type="submit" class="w3-button w3-blue">
                    {{ __('Register') }}
                </button>
            </div>
    </form>
</div>

<script>
    document.getElementById("register").addEventListener("submit",function (e) {
        e.preventDefault();
        var email = document.getElementById("email");
        var password = document.getElementById("password");
        var passwordConfirm = document.getElementById("password-confirm");
        var regbtn = document.getElementById("regbtn");
        var alerts = document.getElementById("alerts");
        var phone = document.getElementById("phone");
        var name = document.getElementById("name");
        var country = document.getElementById("country");
        if(email.checkValidity() && password.checkValidity()){
            let xhr = new XMLHttpRequest();
            xhr.open("post","{{ route('apiregister') }}",true);
            xhr.onreadystatechange = function (){
                if(this.readyState===1){
                    regbtn.setAttribute("disabled",true);
                }
                if(this.readyState===4){
                    console.log(this.response);
                    if(this.status===201){
                        // setCookie("access_token",JSON.parse(this.response).token);
                        window.location.href="{{env("APP_URL")}}/shop/login";
                    }else if(this.status >= 400){
                        let response = JSON.parse(this.response);
                        alerts.innerHTML= "";

                        if(response.email !== undefined){
                            for(i=0;i<response.email.length;i++){
                                var alertel = document.createElement("li");
                                alertel.innerHTML = response.email[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (response.password !== undefined) {
                            for (i = 0; i < response.password.length; i++) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = response.password[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (response.name !== undefined) {
                            for (i = 0; i < response.name.length; i++) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = response.name[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (response.phone !== undefined) {
                            for (i = 0; i < response.phone.length; i++) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = response.phone[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (response.message !== undefined) {
                            var alertel = document.createElement("li");
                            alertel.innerHTML = response.message;
                            alerts.appendChild(alertel);
                        }
                        document.getElementById("wrapper").scrollIntoView();
                    }
                    regbtn.removeAttribute("disabled");
                }
            }

            //send the login request
            xhr.setRequestHeader("content-type","application/x-www-form-urlencoded");
            var reqdata = "email=" + email.value + "&password=" + password.value+ "&password_confirmation=" + passwordConfirm.value+ "&name=" + name.value+ "&phone=" + phone.value+ "&country=" + country.value;
            reqdata = reqdata + "&client_key=" + window.localStorage.getItem("client_key");
            xhr.send(reqdata);
        }else{
            console.log("invalid");
        }
        console.log("trigger");
    });
</script>
@endsection
