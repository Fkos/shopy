<!DOCTYPE html>
<html lang="en">
<head>
    <title>Shopy</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('head')
    <link rel="stylesheet" href="{{asset('css/w3.css')}}">
    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    <script src="{{asset('js/all.min.js')}}"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Raleway:ital@0;1&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Crete+Round&display=swap" rel="stylesheet">
    <script src="{{asset('js/templates.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="/DataTables/datatables.min.css"/>
    <script type="text/javascript" src="/DataTables/datatables.min.js"></script>
    <style>
        body {font-family: 'Raleway', sans-serif;font-size:16px;}
        h1,h2,h3,h4,h5 {font-family: 'Crete Round', serif;}
        .w3-button {border-radius: 6px;}
        input,select {max-width: 500px;transition: 0.4s;}
        @media screen and (max-width:994px){
            .w3-main{
                padding-top: 60px;
            }
        }
        #footer {
            position: fixed;
            bottom: 0px;
            max-height: 60px;
            min-width: 100%;
            padding-bottom: 10px;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
    <script>
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        if(window.location.href !== "{{env('APP_URL')}}/shop/login" && window.location.href !== "{{env('APP_URL')}}/shop/register" && getCookie("access_token") === ""){
            window.location.href = "{{env('APP_URL')}}/shop/login";
        }
        if(window.localStorage.getItem("client_key") === null){
            let xhr = new XMLHttpRequest();
            xhr.open("get","{{route('apikey')}}");
            xhr.onreadystatechange = function () {
                if (this.readyState === 4 && ( this.status === 200 || this.status === 201 )){
                    jres = JSON.parse(this.response);
                    window.localStorage.setItem("client_key",jres.key);
                    console.log(window.localStorage.getItem("client_key"));
                }else{
                    console.log(this.status);
                }
            }
            xhr.send();
        }else{

        }
    </script>
</head>
<body>
    <div id="loadingModal" class="w3-modal" style="min-width: 100%;min-height: 100%;background-color: rgba(255,255,255,0.5);position: absolute;top: 0;left: 0;">
        <div class="w3-modal-content">
            <div class="w3-container">
                <span class="w3-spin" style="position: absolute;left: 49%;top: 49%;"><i class="fas fa-spinner" style="font-size: 1.9rem;animation: spin 2s linear infinite;"></i></span>
            </div>
        </div>
    </div>
    <!-- Sidebar/menu -->
    <nav class="w3-sidebar w3-blue w3-collapse w3-top w3-large w3-padding" style="z-index:3;width:300px;font-weight:bold;" id="mySidebar"><br>
        <a href="javascript:void(0)" onclick="w3_close()" class="w3-button w3-hide-large w3-display-topleft" style="width:100%;font-size:22px;text-align: right;">X</a>
        <div class="w3-container">
            <h3 class="w3-padding-16"><b>{{env('APP_NAME')}}</b></h3>
        </div>
        <div id="sidemenu" class="w3-bar-block">
            <a class="w3-bar-item w3-button w3-hover-white" href="/shop/login" onclick="w3_close()">Connexion</a>
            <a class="w3-bar-item w3-button w3-hover-white" href="/shop/register" onclick="w3_close()">Inscription</a>
        </div>
    </nav>

    <!-- Top menu on small screens -->
    <header class="w3-container w3-top w3-hide-large w3-blue w3-xlarge w3-padding">
        <a href="javascript:void(0)" class="w3-button w3-blue w3-margin-right" onclick="w3_open()">☰</a>
        <span>{{env('APP_NAME')}}</span>
    </header>

    <!-- Overlay effect when opening sidebar on small screens -->
    <div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

    <!-- !PAGE CONTENT! -->
    <div id="wrapper" class="w3-main" style="margin-left:300px;margin-right:0px">
        <div id="main-container" class="w3-container" style="margin:0px !important;">
            @yield('page_title')
            @yield('content')
        </div>
        <footer class="w3-margin-top w3-container">
            <p class="w3-left">© {{ (now()->year == 2020)? now()->year : "2020 - ".now()->year }} <a class="w3-hover-opacity" href="#"> {{env('APP_NAME')}}</a> Propulsé par <a href="#" title="Kosson Fanidji Kassigni" target="_blank" class="w3-hover-opacity">Kosson F. K.</a></p>
        </footer>
    </div>

    <script>
    // Script to open and close sidebar

    function w3_open() {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";
    }

    function w3_close() {
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("myOverlay").style.display = "none";
    }
    </script>
    <script src="{{asset('js/shops.js')}}"></script>
    <script src="{{asset('js/products.js')}}"></script>
    <script src="{{asset('js/control.js')}}"></script>
</body>
</html>
