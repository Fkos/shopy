<!DOCTYPE html>
<html lang="fr">
<title>Shopy</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
@yield('head')
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="{{asset('css/all.min.css')}}">
{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> --}}
<script src="{{asset('js/all.min.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
<style>
body,h1,h2,h3,h4,h5 {font-family: "Poppins", sans-serif}
body {font-size:16px;}
input,select {max-width: 500px;transition: 0.4s;}
@media screen and (max-width:994px){
	.w3-main{
		padding-top: 60px;
	}
}
</style>
<body>

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-blue w3-collapse w3-top w3-large w3-padding" style="z-index:3;width:300px;font-weight:bold;" id="mySidebar"><br>
  <a href="javascript:void(0)" onclick="w3_close()" class="w3-button w3-hide-large w3-display-topleft" style="width:100%;font-size:22px;text-align: right;">X</a>
  <div class="w3-container">
    <h3 class="w3-padding-16"><b>{{env('APP_NAME')}}</b></h3>
  </div>
  <div class="w3-bar-block">
	@yield('sidemenu-top')
	@if(isset($signedin) && $signedin == true)
	<a class="w3-bar-item w3-button w3-hover-white" href="#" onclick="w3_close()"><i class="fas fa-home"></i> Mes Commerces</a>
	<a class="w3-bar-item w3-button w3-hover-white" href="#" onclick="w3_close()"><i class="fas fa-door-open"></i> Tableau de bord</a>
	<a class="w3-bar-item w3-button w3-hover-white" href="#" onclick="w3_close()"><i class="fas fa-calendar"></i> Produits</a>
	<a class="w3-bar-item w3-button w3-hover-white" href="#" onclick="w3_close()"><i class="fas fa-envelope"></i> Ventes</a>
    <a class="w3-bar-item w3-button w3-hover-white" href="#" onclick="w3_close()">Achats</a>
    <a class="w3-bar-item w3-button w3-hover-white" href="#" onclick="w3_close()">Clients</a>
    <a class="w3-bar-item w3-button w3-hover-white" href="#" onclick="w3_close()">Personnel</a>
	@else
    <a class="w3-bar-item w3-button w3-hover-white" href="/login" onclick="w3_close()">Connexion</a>
    <a class="w3-bar-item w3-button w3-hover-white" href="/register" onclick="w3_close()">Inscription</a>
	@endif
    @yield('sidemenu')
  </div>
</nav>

<!-- Top menu on small screens -->
<header class="w3-container w3-top w3-hide-large w3-blue w3-xlarge w3-padding">
  <a href="javascript:void(0)" class="w3-button w3-blue w3-margin-right" onclick="w3_open()">☰</a>
  <span>{{env('APP_NAME')}}</span>
</header>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-right:0px">
	<div class="w3-container" style="margin:0px !important;">

	@yield('content')
	<div class="w3-container w3-light-grey w3-padding-32">
		<p class="w3-left">© {{ (now()->year == 2019)? now()->year : "2019 - ".now()->year }} <a class="w3-hover-opacity" href="#"> Shopy</a>
		<br>Propulsé par <a href="#" title="Kosson's Services" target="_blank" class="w3-hover-opacity">Kosson's services</a></p>
	</div>
	</div>
</div>


	<script>
		// Script to open and close sidebar
		function w3_open() {
			document.getElementById("mySidebar").style.display = "block";
			document.getElementById("myOverlay").style.display = "block";
		}

		function w3_close() {
			document.getElementById("mySidebar").style.display = "none";
			document.getElementById("myOverlay").style.display = "none";
		}
	</script>

</body>
</html>
