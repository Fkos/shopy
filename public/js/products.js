window.ProductManager = {
    
    "findProductById" : function (id) {
        let list = JSON.parse(window.localStorage.getItem("productList"));
        let result = {};
        for(let i = 0;i < list.length ; i++){
            if(list[i].id === id){
                result = list[i];
                break;
            }
        }
        return result;
    },

    
    /*
    *   Charge la liste des produits d'un commerce preselectionne depuis le serveur*/
    "loadProducts" : function () {
        if(getCookie('access_token')!==""){
            w3_close();
            document.getElementById("main-container").innerHTML = window.templates.productWindow;
            this.loadProductForm();

            let xmlHttpRequest = new XMLHttpRequest();
            xmlHttpRequest.open("get", "/api/manage/products?client_key=" + window.localStorage.getItem("client_key"));
            xmlHttpRequest.onreadystatechange = function () {
                if (this.readyState === 1) {
                    document.getElementById("loadingModal").style.display = "block";
                } else if (this.readyState === 4) {
                    if (this.status === 200) {
                        window.localStorage.setItem("productList",this.response);
                    }
                    document.getElementById("loadingModal").style.display = "none";
                }
            };
            xmlHttpRequest.send();
        }
    },

    "loadProductForm" : function (id="") {
        if (id === "") {
            document.getElementById("productFormContainer").innerHTML = window.templates.productForm();
        }else {
            document.getElementById("productFormContainer").innerHTML = window.templates.productForm(id);
        }
        window.ProductManager.setProductFormEvent();
    },

    /*
    *   soumet le formulaire dajout de produits
    *   ne recoit pas de param
    *   re retourne pas de result*/
    "setProductFormEvent" : function () {
        document.getElementById("productForm").onsubmit = function (e) {
            e.preventDefault();
            document.getElementById("save-product-button").setAttribute("disabled","true");
            document.getElementById("loadingModal").style.display = "block";
            let id = document.getElementById("id").value;
            let name = document.getElementById("productname").value;
            let description = document.getElementById("productdescription").value;
            let shop_id = document.getElementById("productshop").value;
            let category_id = document.getElementById("productcategory").value;
            let price = document.getElementById("productprice").value;
            let xmlHttpRequest = new XMLHttpRequest();
            (id === "")?
                xmlHttpRequest.open("post", "/api/manage/products")
                :
                xmlHttpRequest.open("put", `/api/manage/products/${id}`);
            xmlHttpRequest.onreadystatechange = function () {
                switch (this.readyState) {
                    case 1 :
                        break;
                    case 4 :
                        var alerts = document.getElementById("productalerts");
                        if (this.status === 201 || this.status === 200) {
                            loadProducts();
                        } else if (this.status >= 400 && this.status < 500) {
                            alerts.innerHTML = "";
                            var errorMessage = JSON.parse(this.response);
                            if (errorMessage.name !== undefined) {
                                for (i = 0; i < errorMessage.name.length; i++) {
                                    var alertel = document.createElement("li");
                                    alertel.innerHTML = errorMessage.name[i];
                                    alerts.appendChild(alertel);
                                }
                            }
                            if (errorMessage.price !== undefined) {
                                for (i = 0; i < errorMessage.price.length; i++) {
                                    var alertel = document.createElement("li");
                                    alertel.innerHTML = errorMessage.price[i];
                                    alerts.appendChild(alertel);
                                }
                            }
                            if (errorMessage.shop_id !== undefined) {
                                for (i = 0; i < errorMessage.shop_id.length; i++) {
                                    var alertel = document.createElement("li");
                                    alertel.innerHTML = errorMessage.shop_id[i];
                                    alerts.appendChild(alertel);
                                }
                            }
                            if (errorMessage.category_id !== undefined) {
                                for (i = 0; i < errorMessage.category_id.length; i++) {
                                    var alertel = document.createElement("li");
                                    alertel.innerHTML = errorMessage.category_id[i];
                                    alerts.appendChild(alertel);
                                }
                            }
                            if (errorMessage.description !== undefined) {
                                for (i = 0; i < errorMessage.description.length; i++) {
                                    var alertel = document.createElement("li");
                                    alertel.innerHTML = errorMessage.description[i];
                                    alerts.appendChild(alertel);
                                }
                            }
                            if (errorMessage.message !== undefined) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = errorMessage.message;
                                alerts.appendChild(alertel);
                            }
                        } else if (this.status === 500) {
                            alerts.innerHTML = "";
                            var alertel = document.createElement("li");
                            alertel.innerHTML = "Server Error!";
                            alerts.appendChild(alertel);
                        }

                        document.getElementById("loadingModal").style.display = "none";
                        document.getElementById("save-product-button").removeAttribute("disabled");
                        break;
                }
            };
            xmlHttpRequest.setRequestHeader("content-type","application/x-www-form-urlencoded");
            var requestData = "name=" + name + "&price=" + price + "&description=" + description + "&category_id=" + category_id + "&shop_id=" + shop_id;
            requestData = requestData + "&client_key=" + window.localStorage.getItem("client_key");
            xmlHttpRequest.send(requestData);
        };
    },

    /*
    *   Affiche la liste des produit dans un tableau
    *   Recoit en parametre un jsonArray contenant la liste des produits
    *   ne retourne aucune valeur
    * */
    "showProducts" : function (){
        document.getElementById("main-container").innerHTML = window.templates.productWindow;
        let productList = JSON.parse(window.localStorage.getItem("productList"));
        if (productList.length>0){
            let container = document.getElementById("product-container");
            container.innerHTML = window.templates.productTable;
            let table = document.getElementById("producttbody");
            productList.forEach(product => {
                table.insertAdjacentHTML("beforeend",window.templates.productTableItem(product.name,product.price,product.shop,product.purchases,product.sales,product.number,product.id))
            });
        }else{
            document.getElementById("product-container").innerHTML = window.templates.noProductMessage;
        }
        $("document").ready(function(){
            $("#producttable").DataTable();
        });
    },

    /*
    *   Ajoute un produit la liste des produits dans un tableau
    *   Recoit en parametre un jsonObject du produit
    *   ne retourne aucune valeur
    * */
    "showProduct" : function (product){
        console.log(product);
    },

    /*
    *   Affiche le formulaire d'ajout ou modification de produit
    *   Recoit en parametre id du produit ou void
    *   ne retourne aucune valeur
    * */
    "showProductForm" : function (id="") {
        this.loadProductForm(id);
        document.getElementById("productFormModal").style.display = "block";
    },

    /*
    *   Supprime le produit
    *   Recoit en parametre l'event et id du produit ou void
    *   ne retourne aucune valeur
    * */
    "deleteProduct" : function (e,id){
        let target = e.target;
        console.log(target);
        if(confirm("Do you really want to delete this product?")){
            let xmlHttpRequest = new XMLHttpRequest();
            xmlHttpRequest.open("delete","/api/manage/products/"+id);
            xmlHttpRequest.onreadystatechange = function () {
                switch (this.readyState){
                    case 1 :
                        target.setAttribute("disable",true);
                        break;
                    case 4 :
                        if (this.status === 200){
                            document.getElementById("product-container").insertAdjacentHTML("beforebegin",`<div class="w3-panel w3-green  w3-display-container" style="display: block">
                                <span onclick="this.parentElement.style.display='none'" class="w3-button w3-display-topright">X</span>
                                <h3>Success!</h3>
                                <p>The product has been successfully deleted!</p>
                            </div>`);
                            let productList = JSON.parse(window.localStorage.getItem("productList"));
                            productList.forEach((product,index) => {
                                if (product.id === id){
                                    console.log(index);
                                    delete productList[index];
                                    window.localStorage.setItem("productList",JSON.stringify(productList));
                                    showProducts(productList);
                                }
                            });
                        }else{
                            console.log(this.status + " \n" + this.response);
                        }
                        break;
                }
            };
            let jsonRequestData = {};
            jsonRequestData.client_key = window.localStorage.getItem("client_key");
            jsonRequestData = JSON.stringify(jsonRequestData);
            xmlHttpRequest.setRequestHeader("Content-type","application/json");
            xmlHttpRequest.send(jsonRequestData);
        }
    }
}