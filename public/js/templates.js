window.templates = {
    "shopItem" : function (shop){
        return `
        <div class="w3-margin-right w3-margin-top w3-border w3-third" style="max-width: 250px; border-radius: 6px" >
            <div class="w3-container">
                <h3 style="text-decoration: underline;">${shop.name}</h3>
            </div>
            <div class="w3-container">
                <p>${shop.phone}</p>
                <p>${shop.email}</p>
                <p>${shop.address}</p>
            </div>
            <div class="w3-row w3-padding">
                <button style="margin-right: 10px" class="w3-button w3-light-blue w3-cell" title="Dashboard"><i class="fa fa-chart-pie"></i> </button>
                <button style="margin-right: 10px" class="w3-button w3-yellow w3-cell" title="Edit this shop"><i class="fa fa-pencil-alt"></i> </button>
                <button style="margin-right: 10px" class="w3-button w3-red w3-cell" title="Delete this shop" onclick="window.ShopManager.deleteShop(event,${shop.id})"><i class="fa fa-trash"></i> </button>
            </div>
        </div>`
    },

    "sideMenu" : `
        <a class="w3-bar-item w3-button w3-hover-white" onclick="window.ShopManager.showShops();w3_close()"><i class="fas fa-store"></i> My Shops</a>
        <a class="w3-bar-item w3-button w3-hover-white" onclick="w3_close()"><i class="fas fa-users"></i> Employees</a>
        <a class="w3-bar-item w3-button w3-hover-white" onclick="window.ProductManager.showProducts();w3_close()"><i class="fas fa-list"></i> Products</a>
        <a class="w3-bar-item w3-button w3-hover-white" onclick="w3_close()"><i class="fas fa-truck"></i> Providers</a>
        <a class="w3-bar-item w3-button w3-hover-white" onclick="showPurchases();w3_close()"><i class="fas fa-chart-line"></i> Purchases</a>
        <a class="w3-bar-item w3-button w3-hover-white" onclick="w3_close()"><i class="fas fa-hands-helping"></i> Customers</a>
        <a class="w3-bar-item w3-button w3-hover-white" onclick="w3_close()"><i class="fas fa-chart-line"></i> Sales</a>
    `,

    "shopWindow" : `
        <div id="addFormModal" class="w3-modal">
            <div class="w3-modal-content">
                <div class="w3-container">
                    <span onclick="document.getElementById('addFormModal').style.display='none'"
                    class="w3-button w3-display-topright">&times;</span>
                    <h2>Add your Shop</h2>
                    <ul class="w3-alert w3-margin-bottom-16 w3-red" id="shopalerts"  style="max-width: 600px"></ul>
                    <form id="addShopForm">
                        <div class="w3-margin-bottom">
                            <label for="shopname">Shop Name :</label>
                            <input type="text" class="w3-input" id="shopname">
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="shopemail">E-mail :</label>
                            <input type="email" class="w3-input" id="shopemail">
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="shopphone">Phone :</label>
                            <input type="text" max="10" class="w3-input" id="shopphone">
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="shopaddress">Address :</label>
                            <input type="text" class="w3-input" id="shopaddress">
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="shopcountry">Country of location :</label>
                            <select type="text" class="w3-input" id="shopcountry">
                                <option selected disabled>Select a Country</option>
                                <option value="1">Togo</option>
                            </select>
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="shopdescription">Description :</label>
                            <textarea class="w3-input w3-border" id="shopdescription" rows="5"></textarea>
                        </div>
                        <input id="shoplatitude" value="0" hidden>
                        <input id="shoplongitude" value="0" hidden>
                        <div class="w3-margin-bottom">
                            <button type="submit" class="w3-button w3-hover-white w3-green" id="save-shop-button">Save</button>
                            <button type="reset" class="w3-button w3-hover-white w3-red" id="reset-shop-button">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <h1>Your shops</h1>
        <div class="top-menu">
            <button class="w3-button w3-border w3-hover-white w3-hover-border-black w3-green" id="add-shop-button" onclick="document.getElementById('addFormModal').style.display='block'"><i class="fa fa-plus"></i> Add a Shop</button>
        </div>
        <div class="w3-row" id="shop-container">

        </div>
    `,

    "productWindow":`
        <h1>Your products</h1>
        <div class="w3-row">
            <div id="productFormContainer"></div>
            <div class="w3-container w3-margin-bottom">
                <button class="w3-button w3-hover-white w3-green" id="add-product-button" onclick="window.ProductManager.showProductForm()"><i class="fa fa-plus"></i> Add a Product</button>
            </div>
        </div>
        <div id="product-container" style="overflow-x: scroll">

        </div>
    `,
    "productForm" : function (id="") {
        let name = "";
        let price = "";
        let category = "";
        let shop = "";
        let description = "";
        let action = "Add";
        let shop_id = "";
        let category_id = "";
        if (id !== ""){
            action = "Edit";
            let productList = JSON.parse(window.localStorage.getItem("productList"));
            productList.forEach(product => {
                if (product.id === id){
                    name = product.name;
                    price = product.price;
                    category = product.category;
                    shop = product.shop;
                    description = product.description;
                    shop_id = product.shop_id;
                    category_id = product.category_id;
                }
            });
        }
        return `
        <div id="productFormModal" class="w3-modal">
            <div class="w3-modal-content">
                <div class="w3-container">
                    <span onclick="document.getElementById('productFormModal').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                    <h2 id="productFormTitle">${action} a new Product</h2>
                    <ul class="w3-alert w3-margin-bottom-16 w3-red" id="productalerts" style="max-width:600px"></ul>
                    <form id="productForm">
                        <input type="hidden" id="id" value="${id}">
                        <div class="w3-margin-bottom">
                            <label for="productname">Product Name :</label>
                            <input type="text" class="w3-input" id="productname" value="${name}">
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="productprice">Price :</label>
                            <input type="text" class="w3-input" id="productprice" value="${price}">
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="productcategory">Category :</label>
                            <select type="text" class="w3-input" id="productcategory">
                                <option selected disabled>Select a Category</option>
                                ${categoryList(category_id)}
                            </select>
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="productshop">Shop where it is sold :</label>
                            <select type="text" class="w3-input" id="productshop">
                                <option selected disabled>Select a Shop</option>
                                ${shopList(shop_id)}
                            </select>
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="productdescription">Description :</label>
                            <textarea class="w3-input w3-border" id="productdescription" rows="5">${description}</textarea>
                        </div>
                        <div class="w3-margin-bottom">
                            <button type="submit" class="w3-button w3-hover-white w3-green" id="save-product-button">Save</button>
                            <button type="reset" class="w3-button w3-hover-white w3-red" id="reset-product-button">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>`;
    },
    "productTable" : `
    <table class="w3-table-all" id="producttable">
        <thead>
            <tr>
                <th>Shop Name</th>
                <th>Product Name</th>
                <th>Price</th>
                <th>Purchases</th>
                <th>Sales</th>
                <th>Available</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody id="producttbody">

        </tbody>
    </table>
    `,
    "noProductMessage" : `<div class="w3-panel w3-light-blue w3-padding" style="max-width:500px;"><h3>Info</h3><p>There is not any product saved in this shop!</p></div>`,

    "productTableItem" : function(name,price,shopName,purchase,sales,available,id){
        return `
        <tr>
        <td>${shopName}</td>
        <td>${name}</td>
        <td>${price}</td>
        <td>${purchase}</td>
        <td>${sales}</td>
        <td>${available}</td>
        <td>
            <div class="w3-row">
                <div class="w3-cell w3-padding-small">
                    <button class="w3-button w3-yellow" onclick="window.ProductManager.showProductForm(${id})"><i class="fa fa-pencil-alt"></i></button>
                </div>
                <div class="w3-cell w3-padding-small">
                    <button class="w3-button w3-red"  onclick="window.ProductManager.deleteProduct(event,${id})"><i class="fa fa-trash"></i></button>
                </div>
            </div>
        </td>
        </tr>
        `;
    },

    "purchaseWindow":`
        <h1>Your purchases</h1>
        <div class="w3-row">
            <div id="purchaseFormContainer"></div>
            <div class="w3-container w3-margin-bottom">
                <button class="w3-button w3-hover-white w3-green" id="add-purchase-button" onclick="showPurchaseForm()"><i class="fa fa-plus"></i> Add a Purchase</button>
            </div>
        </div>
        <div id="purchase-container" style="overflow-x: scroll">

        </div>
    `,
    "purchaseForm" : function (id="") {
        let price = "";
        let quantity = "";
        let product_id = "";
        let provider_id = "";
        let provider_name = "";
        let provider_email = "";
        let provider_phone = "";
        let action = "Add";
        if (id !== ""){
            action = "Edit";
            let purchaseList = JSON.parse(window.localStorage.getItem("purchaseList"));
            purchaseList.forEach(purchase => {
                if (purchase.id === id){
                    provider_id = purchase.provider_id;
                    price = purchase.price;
                    quantity = purchase.quantity;
                    product_id = purchase.product_id;
                }
            });
        }
        return `
        <div id="purchaseFormModal" class="w3-modal">
            <div class="w3-modal-content">
                <div class="w3-container">
                    <span onclick="document.getElementById('purchaseFormModal').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                    <h2 id="purchaseFormTitle">${action} a new Purchase</h2>
                    <ul class="w3-alert w3-margin-bottom-16 w3-red" id="purchasealerts" style="max-width:600px"></ul>
                    <form id="purchaseForm">
                        <input type="hidden" id="id" value="${id}">
                        <div class="w3-margin-bottom">
                            <label for="purchaseproduct_id">Product :</label>
                            <input class="w3-input" list="purchaseproducts" id="purchaseproduct_id">
                            <datalist id="purchaseproducts">
                                <option disabled>Select a Product</option>
                                ${productList(product_id)}
                            </datalist>
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="purchaseprice">Price :</label>
                            <input type="text" class="w3-input" id="purchaseprice" value="${price}">
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="purchasequantity">Units :</label>
                            <input type="number" class="w3-input" id="purchasequantity" value="${quantity}">
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="new_provider">New Provider?</label>
                            <input type="checkbox" id="new_provider" onchange="toggleNewProvider()">
                        </div>
                        <div class="w3-margin-bottom" id="provider_select_container">
                            <h4>Select provider</h4>
                            <label for="purchaseprovider_id">Provider :</label>
                            <select type="text" class="w3-input" id="purchaseprovider_id">
                                <option disabled>Select a Provider</option>
                                ${providerList(provider_id)}
                            </select>
                        </div>
                        <div id="provider_form_container" style="display: none;">
                        <h4>New provider</h4>
                        <div class="w3-margin-bottom">
                            <label for="providername">Provider's name :</label>
                            <input type="text" class="w3-input" id="providername" value="${provider_name}">
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="provideremail">Provider's e-mail :</label>
                            <input type="email" class="w3-input" id="provideremail" value="${provider_email}">
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="providerphone">Provider's phone :</label>
                            <input type="text" class="w3-input" id="providerphone" value="${provider_phone}">
                        </div>
                        </div>
                        <div class="w3-margin-bottom">
                            <button type="submit" class="w3-button w3-hover-white w3-green" id="save-purchase-button">Save</button>
                            <button type="reset" class="w3-button w3-hover-white w3-red" id="reset-purchase-button">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>`;
    },
    "purchaseTable" : `
    <table class="w3-table-all" id="purchasetable">
        <thead>
            <tr>
                <th>Shop Name</th>
                <th>Product Name</th>
                <th>Date</th>
                <th>Price</th>
                <th>Units</th>
                <th>Provider</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody id="purchasetbody">

        </tbody>
    </table>
    `,
    "noPurchaseMessage" : `<div class="w3-panel w3-light-blue w3-padding" style="max-width:500px;"><h3>Info</h3><p>There is not any purchase saved in this shop!</p></div>`,

    "purchaseTableItem" : function(shop,product,date,price,units,provider,id){
        return `
        <tr>
        <td>${shop}</td>
        <td>${product}</td>
        <td>${date}</td>
        <td>${price}</td>
        <td>${units}</td>
        <td>${provider}</td>
        <td>
            <div class="w3-row">
                <div class="w3-cell w3-padding-small">
                    <button class="w3-button w3-yellow" onclick="showPurchaseForm(${id})"><i class="fa fa-pencil-alt"></i></button>
                </div>
                <div class="w3-cell w3-padding-small">
                    <button class="w3-button w3-red"  onclick="deletePurchase(event,${id})"><i class="fa fa-trash"></i></button>
                </div>
            </div>
        </td>
        </tr>
        `;
    },
};
function providerList(id=""){
    let result= "";
    list = JSON.parse(window.localStorage.getItem("providerList"));
    list.forEach(item => {
       result += `<option value="${item.id}" ${(id === item.id)?"selected":""}>${item.name}</option>`;
    });
    return result;
}

function productList(id=""){
    let result= "";
    list = JSON.parse(window.localStorage.getItem("productList"));
    list.forEach(item => {
        result += `<option value="${item.id}" ${(id === item.id)?"selected":""}>${item.name}</option>`;
    });
    return result;
}

function categoryList(id=""){
    let result= "";
    list = JSON.parse(window.localStorage.getItem("categories"));
    list.forEach(item => {
        result += `<option value="${item.id}" ${(id === item.id)?"selected":""}>${item.name}</option>`;
    });
    return result;
}

function toggleNewProvider() {
    if (document.getElementById("new_provider").checked){
        document.getElementById("provider_form_container").style.display = "block";
        document.getElementById("provider_select_container").style.display = "none";
    }else{
        document.getElementById("provider_form_container").style.display = "none";
        document.getElementById("provider_select_container").style.display = "block";
    }
}

function shopList(id=""){
    let result= "";
    if(window.localStorage.getItem("shopList")!==null){
    list = JSON.parse(window.localStorage.getItem("shopList"));
    list.forEach(item => {
        result += `<option value="${item.id}" ${(id === item.id)?"selected":""}>${item.name}</option>`;
    });
    }
    return result;
}
