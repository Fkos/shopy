if(getCookie('access_token')!==""){
    checkAccess();
    loadCategories();
    window.ShopManager.loadShops();
    window.ProductManager.loadProducts();
    loadPurchases();
    loadProviders();
    loadCustomers();
    loadSales();
}

function hasJsonStructure(str) {
    if (typeof str !== 'string') return false;
    try {
        const result = JSON.parse(str);
        const type = Object.prototype.toString.call(result);
        return type === '[object Object]'
            || type === '[object Array]';
    } catch (err) {
        return false;
    }
}

function loadMenu() {
    if(getCookie("access_token")!==""){
    document.getElementById("sidemenu").innerHTML = window.templates.sideMenu;
    }
}


function findPurchaseById(id) {
    let list = JSON.parse(window.localStorage.getItem("purchaseList"));
    let result = {};
    for(let i = 0;i < list.length ; i++){
        if(list[i].id === id){
            result = list[i];
            break;
        }
    }
    return result;
}

function findSaleById(id) {
    let list = JSON.parse(window.localStorage.getItem("saleList"));
    let result = {};
    for(let i = 0;i < list.length ; i++){
        if(list[i].id === id){
            result = list[i];
            break;
        }
    }
    return result;
}

function findProviderById(id) {
    let list = JSON.parse(window.localStorage.getItem("providerList"));
    let result = {};
    for(let i = 0;i < list.length ; i++){
        if(list[i].id === id){
            result = list[i];
            break;
        }
    }
    return result;
}

function findCustomerById(id) {
    let list = JSON.parse(window.localStorage.getItem("customerList"));
    let result = {};
    for(let i = 0;i < list.length ; i++){
        if(list[i].id === id){
            result = list[i];
            break;
        }
    }
    return result;
}


function checkAccess(){
    let xmlHttpRequest = new XMLHttpRequest();
    let url = "/api/checkaccess?client_key="+window.localStorage.getItem("client_key")+"&access_token="+getCookie("access_token");
    xmlHttpRequest.open("get",url);
    xmlHttpRequest.onreadystatechange = function (){
        if (this.readyState === 4) {
            console.log("status "+this.status);
            if(this.status === 200){
                loadMenu();
                if(hasJsonStructure(this.response)){
                    window.user = JSON.parse(this.response);
                    document.getElementById('sidemenu').insertAdjacentHTML("afterbegin",`<a class="w3-bar-item w3-button w3-hover-white" href="javascript:void(0)"><i class="fas fa-user-alt"></i> ${(window.user.name.length<16)?window.user.name:window.user.name.slice(0,15) + ". . ."}</a>`);
                }
            }else{
                redirectLogin();
            }
        }
    }
    xmlHttpRequest.send();
}

function loadCategories(){
    let xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.open("get","/api/manage/categories");
    xmlHttpRequest.onreadystatechange = function (){
        if(this.readyState === 4){
            if(this.status === 200){
                if(hasJsonStructure(this.response)){
                    window.localStorage.setItem("categories",this.response);
                }
            }
        }
    };
    xmlHttpRequest.send();
}

function redirectLogin(){
    document.cookie = "access_token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    window.location.href = "/shop/login";
}

/*
*   Charge la liste des achats d'un commerce preselectionne depuis le serveur*/
function loadPurchases() {
    if(getCookie('access_token')!==""){
        w3_close();
        document.getElementById("loadingModal").style.display = "block";
        let xmlHttpRequest = new XMLHttpRequest();
        xmlHttpRequest.open("get", "/api/manage/purchases?client_key=" + window.localStorage.getItem("client_key"));
        xmlHttpRequest.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    window.localStorage.setItem("purchaseList",this.response);
                }else{
                    console.log('error loading purchases\n',this.response);
                }
            }
        };
        xmlHttpRequest.send();
    }
}

function loadPurchaseForm(id="") {
    if (id === "") {
        document.getElementById("purchaseFormContainer").innerHTML = window.templates.purchaseForm();
    }else {
        document.getElementById("purchaseFormContainer").innerHTML = window.templates.purchaseForm(id);
    }
    submitPurchaseForm();
}

/*
*   soumet le formulaire dajout de produits
*   ne recoit pas de param
*   re retourne pas de result*/
function submitPurchaseForm() {
    console.log("submit event");
    document.getElementById("purchaseForm").onsubmit = function (e) {
        e.preventDefault();
        document.getElementById("save-purchase-button").setAttribute("disabled","true");
        document.getElementById("purchaseFormModal").style.display = "none";
        document.getElementById("loadingModal").style.display = "block";
        let id = document.getElementById("id").value;
        let product_id = document.getElementById("purchaseproduct_id").value;
        let provider_id = document.getElementById("purchaseprovider_id").value;
        let quantity = document.getElementById("purchasequantity").value;
        let price = document.getElementById("purchaseprice").value;
        let provider_name = document.getElementById("providername").value;
        let provider_email = document.getElementById("provideremail").value;
        let provider_phone = document.getElementById("providerphone").value;
        let new_provider = document.getElementById("new_provider");
        let xmlHttpRequest = new XMLHttpRequest();
        (id === "")?
            xmlHttpRequest.open("post", "/api/manage/purchases")
            :
            xmlHttpRequest.open("put", `/api/manage/purchases/${id}`);
        xmlHttpRequest.onreadystatechange = function () {
            let response = this.response;
            switch (this.readyState) {
                case 1 :
                    break;
                case 4 :
                    var alerts = document.getElementById("purchasealerts");
                    if (this.status === 201 || this.status === 200) {
                        loadPurchases();
                    } else if (this.status >= 400 && this.status < 500) {
                        alerts.innerHTML = "";
                        var errorMessage = JSON.parse(this.response);
                        if (errorMessage.price !== undefined) {
                            for (i = 0; i < errorMessage.price.length; i++) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = errorMessage.price[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (errorMessage.quantity !== undefined) {
                            for (i = 0; i < errorMessage.quantity.length; i++) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = errorMessage.quantity[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (errorMessage.product_id !== undefined) {
                            for (i = 0; i < errorMessage.product_id.length; i++) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = errorMessage.product_id[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (errorMessage.provider_id !== undefined) {
                            for (i = 0; i < errorMessage.provider_id.length; i++) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = errorMessage.provider_id[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (errorMessage.provider_name !== undefined) {
                            for (i = 0; i < errorMessage.provider_name.length; i++) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = errorMessage.provider_name[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (errorMessage.provider_email !== undefined) {
                            for (i = 0; i < errorMessage.provider_email.length; i++) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = errorMessage.provider_email[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (errorMessage.provider_phone !== undefined) {
                            for (i = 0; i < errorMessage.provider_phone.length; i++) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = errorMessage.provider_phone[i];
                                alerts.appendChild(alertel);
                            }
                        }
                        if (errorMessage.message !== undefined) {
                            var alertel = document.createElement("li");
                            alertel.innerHTML = errorMessage.message;
                            alerts.appendChild(alertel);
                        }
                    } else if (this.status === 500) {
                        alerts.innerHTML = "";
                        var alertel = document.createElement("li");
                        alertel.innerHTML = "Server Error!";
                        alerts.appendChild(alertel);
                    }

                    document.getElementById("loadingModal").style.display = "none";
                    document.getElementById("purchaseFormModal").style.display = "block";
                    document.getElementById("save-purchase-button").removeAttribute("disabled");
                    break;
            }
        };
        xmlHttpRequest.setRequestHeader("content-type","application/x-www-form-urlencoded");
        var requestData = "price=" + price + "&quantity=" + quantity + "&product_id=" + product_id;
        if(!new_provider.checked){
            requestData += "&provider_id=" + provider_id ;
        }else{
            requestData += "&provider_email=" + provider_email + "&provider_phone=" + provider_phone + "&provider_name=" + provider_name + "&new_provider=" + new_provider;
        }
        requestData = requestData + "&client_key=" + window.localStorage.getItem("client_key");
        xmlHttpRequest.send(requestData);
    };

}

/*
*   Affiche la liste des produit dans un tableau
*   Recoit en parametre un jsonArray contenant la liste des produits
*   ne retourne aucune valeur
* */
function showPurchases(){
    document.getElementById("main-container").innerHTML = window.templates.purchaseWindow;
    let purchaseList = JSON.parse(window.localStorage.getItem("purchaseList"));
    if (purchaseList.length>0){
        let container = document.getElementById("purchase-container");
        container.innerHTML = window.templates.purchaseTable;
        let table = document.getElementById("purchasetbody");
        purchaseList.forEach(purchase => {
            table.insertAdjacentHTML("beforeend",window.templates.purchaseTableItem(findProductById(purchase.product_id).shop,findProductById(purchase.product_id).name,new Date(purchase.date).toLocaleString(),purchase.price,purchase.quantity,findProviderById(purchase.provider_id).name,purchase.id))
        });
    }else{
        document.getElementById("purchase-container").innerHTML = window.templates.noPurchaseMessage;
    }
    $("document").ready(function(){
        $("#purchasetable").DataTable();
    });
}

/*
*   Ajoute un produit la liste des produits dans un tableau
*   Recoit en parametre un jsonObject du produit
*   ne retourne aucune valeur
* */
function showPurchase(purchase){
    console.log(purchase);
}

/*
*   Affiche le formulaire d'ajout ou modification de produit
*   Recoit en parametre id du produit ou void
*   ne retourne aucune valeur
* */
function showPurchaseForm(id="") {
    loadPurchaseForm(id);
    document.getElementById("purchaseFormModal").style.display = "block";
}

/*
*   Supprime le produit
*   Recoit en parametre l'event et id du produit ou void
*   ne retourne aucune valeur
* */
function deletePurchase(e,id){
    let target = e.target;
    console.log(target);
    if(confirm("Do you really want to delete this purchase?")){
        let xmlHttpRequest = new XMLHttpRequest();
        xmlHttpRequest.open("delete","/api/manage/purchases/"+id);
        xmlHttpRequest.onreadystatechange = function () {
            switch (this.readyState){
                case 1 :
                    target.setAttribute("disable",true);
                    break;
                case 4 :
                    if (this.status === 200){
                        document.getElementById("purchase-container").insertAdjacentHTML("beforebegin",`<div class="w3-panel w3-green  w3-display-container" style="display: block">
                            <span onclick="this.parentElement.style.display='none'" class="w3-button w3-display-topright">X</span>
                            <h3>Success!</h3>
                            <p>The purchase has been successfully deleted!</p>
                        </div>`);
                        let purchaseList = JSON.parse(window.localStorage.getItem("purchaseList"));
                        purchaseList.forEach((purchase,index) => {
                            if (purchase.id === id){
                                console.log(index);
                                delete purchaseList[index];
                                window.localStorage.setItem("purchaseList",JSON.stringify(purchaseList));
                                showPurchases(purchaseList);
                            }
                        });
                    }else{
                        console.log(this.status + " \n" + this.response);
                    }
                    break;
            }
        };
        let jsonRequestData = {};
        jsonRequestData.client_key = window.localStorage.getItem("client_key");
        jsonRequestData = JSON.stringify(jsonRequestData);
        xmlHttpRequest.setRequestHeader("Content-type","application/json");
        xmlHttpRequest.send(jsonRequestData);
    }
}


/*
*   Charge la liste des ventes d'un commerce preselectionne depuis le serveur*/
function loadSales() {
    if(getCookie('access_token')!==""){
        w3_close();
        document.getElementById("loadingModal").style.display = "block";
        let xmlHttpRequest = new XMLHttpRequest();
        xmlHttpRequest.open("get", "/api/manage/sales?client_key=" + window.localStorage.getItem("client_key"));
        xmlHttpRequest.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    window.localStorage.setItem("saleList",this.response);
                }
                document.getElementById("loadingModal").style.display = "none";
            }
        };
        xmlHttpRequest.send();
    }
}

/*
*   Charge la liste des fournisseurs d'un commerce preselectionne depuis le serveur*/
function loadProviders() {
    if(getCookie('access_token')!==""){
        w3_close();
        document.getElementById("loadingModal").style.display = "block";
        let xmlHttpRequest = new XMLHttpRequest();
        xmlHttpRequest.open("get", "/api/manage/providers?client_key=" + window.localStorage.getItem("client_key"));
        xmlHttpRequest.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    window.localStorage.setItem("providerList",this.response);
                }
                document.getElementById("loadingModal").style.display = "none";
            }
        };
        xmlHttpRequest.send();
    }
}

/*
*   Charge la liste des Clients d'un commerce preselectionne depuis le serveur*/
function loadCustomers() {
    if(getCookie('access_token')!==""){
        w3_close();
        document.getElementById("loadingModal").style.display = "block";
        let xmlHttpRequest = new XMLHttpRequest();
        xmlHttpRequest.open("get", "/api/manage/customers?client_key=" + window.localStorage.getItem("client_key"));
        xmlHttpRequest.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    window.localStorage.setItem("customerList",this.response);
                }
                document.getElementById("loadingModal").style.display = "none";
            }
        };
        xmlHttpRequest.send();
    }
}


/*
*   Charge la liste des Clients d'un commerce preselectionne depuis le serveur*/
function loadEmployees() {
    if(getCookie('access_token')!==""){
        w3_close();
        document.getElementById("loadingModal").style.display = "block";
        let xmlHttpRequest = new XMLHttpRequest();
        xmlHttpRequest.open("get", "/api/manage/employees?client_key=" + window.localStorage.getItem("client_key"));
        xmlHttpRequest.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    window.localStorage.setItem("employeeList",this.response);
                }
                document.getElementById("loadingModal").style.display = "none";
            }
        };
        xmlHttpRequest.send();
    }
}
