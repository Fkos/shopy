window.ShopManager = {
    
    /*
    *   Charge la liste des commerces depuis le serveur*/
    "loadShops" : function (){
        if(getCookie('access_token')!==""){
        w3_close();
        document.getElementById("loadingModal").style.display = "block";
        let xmlHttpRequest = new XMLHttpRequest();
        xmlHttpRequest.open("get","/api/manage/shops?client_key="+window.localStorage.getItem("client_key"));
        xmlHttpRequest.onreadystatechange = function () {
            if (this.readyState === 4){
                if (this.status === 200){
                    if(hasJsonStructure(this.response)){
                        window.localStorage.setItem("shopList",this.response);
                    }
                }
                document.getElementById("loadingModal").style.display = "none";
            }
        };
        xmlHttpRequest.send();
        }
    },


    /*
    *   Ajoute plusieurs commerces a la grille des commerces*/
    "showShops" : function(){
        document.getElementById("main-container").innerHTML = window.templates.shopWindow;
        let shopList = JSON.parse(window.localStorage.getItem("shopList"));
        for (let index=0 ; index < shopList.length ; index++){
            let shop = shopList[index];
            this.showShop(shop);
        }
    },

    "findShopById" : function (id) {
        let list = JSON.parse(window.localStorage.getItem("shopList"));
        let result = {};
        for(let i = 0;i < list.length ; i++){
            if(list[i].id === id){
                result = list[i];
                break;
            }
        }
        return result;
    },
    
    /*
    *   event which submit shop form
    */
    "setShopFormEvent" : function () {
        document.getElementById("addShopForm").onsubmit = function (e) {
            e.preventDefault();
            document.getElementById("addFormModal").style.display = "none";
            document.getElementById("loadingModal").style.display = "block";
            document.getElementById("save-shop-button").setAttribute("disabled", "true");
            let name = encodeURIComponent(document.getElementById("shopname").value);
            let email = encodeURIComponent(document.getElementById("shopemail").value);
            let phone = encodeURIComponent(document.getElementById("shopphone").value);
            let description = encodeURIComponent(document.getElementById("shopdescription").value);
            let country_id = encodeURIComponent(document.getElementById("shopcountry").value);
            let address = encodeURIComponent(document.getElementById("shopaddress").value);
            let latitude = encodeURIComponent(document.getElementById("shoplatitude").value);
            let longitude = encodeURIComponent(document.getElementById("shoplongitude").value);
            let xmlHttpRequest = new XMLHttpRequest();
            xmlHttpRequest.open("post", "/api/manage/shops");
            xmlHttpRequest.onreadystatechange = function () {
                switch (this.readyState) {
                    case 1 :
                        break;
                    case 4 :
                        var alerts = document.getElementById("shopalerts");
                        if (this.status === 201 || this.status === 200) {
                            let shopList = JSON.parse(window.localStorage.getItem("shopList"));
                            let shop = JSON.parse(this.response);
                            shopList.push(shop);
                            window.localStorage.setItem("shopList", JSON.stringify(shopList));
                            document.getElementById("addFormModal").style.display = "none";
                            showShop(shop);
                        } else if (this.status >= 400 && this.status < 500) {
                            alerts.innerHTML = "";
                            var errorMessage = JSON.parse(this.response);
                            if (errorMessage.email !== undefined) {
                                for (i = 0; i < errorMessage.email.length; i++) {
                                    var alertel = document.createElement("li");
                                    alertel.innerHTML = errorMessage.email[i];
                                    alerts.appendChild(alertel);
                                }
                            }
                            if (errorMessage.password !== undefined) {
                                for (i = 0; i < errorMessage.password.length; i++) {
                                    var alertel = document.createElement("li");
                                    alertel.innerHTML = errorMessage.password[i];
                                    alerts.appendChild(alertel);
                                }
                            }
                            if (errorMessage.name !== undefined) {
                                for (i = 0; i < errorMessage.name.length; i++) {
                                    var alertel = document.createElement("li");
                                    alertel.innerHTML = errorMessage.name[i];
                                    alerts.appendChild(alertel);
                                }
                            }
                            if (errorMessage.phone !== undefined) {
                                for (i = 0; i < errorMessage.phone.length; i++) {
                                    var alertel = document.createElement("li");
                                    alertel.innerHTML = errorMessage.phone[i];
                                    alerts.appendChild(alertel);
                                }
                            }
                            if (errorMessage.address !== undefined) {
                                for (i = 0; i < errorMessage.address.length; i++) {
                                    var alertel = document.createElement("li");
                                    alertel.innerHTML = errorMessage.address[i];
                                    alerts.appendChild(alertel);
                                }
                            }
                            if (errorMessage.message !== undefined) {
                                var alertel = document.createElement("li");
                                alertel.innerHTML = errorMessage.message;
                                alerts.appendChild(alertel);
                            }
                            document.getElementById("addFormModal").style.display = "block";
                        } else if (this.status === 500) {
                            alerts.innerHTML = "";
                            var alertel = document.createElement("li");
                            alertel.innerHTML = "Server Error!";
                            alerts.appendChild(alertel);
                            document.getElementById("addFormModal").style.display = "block";
                        }
    
                        document.getElementById("loadingModal").style.display = "none";
                        document.getElementById("save-shop-button").removeAttribute("disabled");
                        break;
                }
            };
            xmlHttpRequest.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            var requestData = "name=" + name + "&email=" + email + "&phone=" + phone + "&address=" + address + "&country_id=" + country_id + "&description=" + description + "&latitude=" + latitude + "&longitude=" + longitude;
            requestData = requestData + "&client_key=" + window.localStorage.getItem("client_key");
            xmlHttpRequest.send(requestData);
        }
    },

    /*
    *   Ajoute un nouveau commerce a la grille des commerces
    */
    "showShop" : function (shop) {
        let shopContainer = document.getElementById("shop-container");
        shopContainer.insertAdjacentHTML("beforeend",window.templates.shopItem(shop));
    },
    
    "deleteShop" : function (e,id){
        let target = e.target;
        if(confirm("Do you really want to delete this shop?")){
            let xmlHttpRequest = new XMLHttpRequest();
            xmlHttpRequest.open("delete","/api/manage/shops/"+id);
            xmlHttpRequest.onreadystatechange = function () {
                switch (this.readyState){
                    case 1 :
                        target.setAttribute("disable",true);
                        break;
                    case 4 :
                        if (this.status === 200){
                            document.getElementById("shop-container").insertAdjacentHTML("beforebegin",`<div class="w3-panel w3-green  w3-display-container" style="display: block">
                                <span onclick="this.parentElement.style.display='none'" class="w3-button w3-display-topright">X</span>
                                <h3>Success!</h3>
                                <p>The shop has been successfully deleted!</p>
                            </div>`);
                            let shopList = JSON.parse(window.localStorage.getItem("shopList"));
                            shopList.forEach((shop,index) => {
                                if (shop.id === id){
                                    console.log(index);
                                    delete shopList[index];
                                    window.localStorage.setItem("shopList",JSON.stringify(shopList));
                                    this.showShops(shopList);
                                }
                            });
                        }else{
                            console.log(this.status + "\n" + this.response);
                        }
                    break;
                }
            };
            let jsonRequestData = {};
            jsonRequestData.client_key = window.localStorage.getItem("client_key");
            jsonRequestData = JSON.stringify(jsonRequestData);
            xmlHttpRequest.setRequestHeader("Content-type","application/json");
            xmlHttpRequest.send(jsonRequestData);
        }
    },
}