<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('insert into countries (id, name,iso,phone_code) values (?, ?, ?, ?)', [1, 'Togo', 'TG', 228])  ;
    }
}
