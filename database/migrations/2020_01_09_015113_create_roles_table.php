<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('can_read_sales')->default(true);
            $table->boolean('can_edit_sales')->default(true);
            $table->boolean('can_create_sales')->default(true);
            $table->boolean('can_delete_sales')->default(true);
            $table->boolean('can_read_shops')->default(true);
            $table->boolean('can_edit_shops')->default(true);
            $table->boolean('can_delete_shops')->default(true);
            $table->boolean('can_read_products')->default(true);
            $table->boolean('can_edit_products')->default(true);
            $table->boolean('can_create_products')->default(true);
            $table->boolean('can_delete_products')->default(true);
            $table->boolean('can_read_users')->default(true);
            $table->boolean('can_edit_users')->default(true);
            $table->boolean('can_create_users')->default(true);
            $table->boolean('can_delete_users')->default(true);
            $table->boolean('can_read_purchases')->default(true);
            $table->boolean('can_edit_purchases')->default(true);
            $table->boolean('can_create_purchases')->default(true);
            $table->boolean('can_delete_purchases')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
